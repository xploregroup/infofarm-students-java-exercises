package model;

public class Person {
    protected String name;

    public Person() {
        this.name = "Jefke";
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
