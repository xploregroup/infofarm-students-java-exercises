package be.infofarm.model;

public abstract class Employee {
    String teamName;

    public Employee(String teamName) {
        this.teamName = teamName;
    }

    String getGreeting() {
        return String.format("I'm glad to be in the %s team!", teamName);
    }
}
