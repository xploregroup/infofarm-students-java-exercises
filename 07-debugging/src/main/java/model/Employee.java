package model;

public class Employee extends Person {
    private String positionString;

    public Employee(String position, String name) {
        super();
        switch (position) {
            case "engineer":
                positionString = "Data Engineer";
            case "scientist":
                positionString = "Data Scientist";
            case "both":
                positionString = "Data Engineer or Data Scientist";
            default:
                throw new IllegalArgumentException("Not a valid position !");
        }
    }

    public void display() {
        System.out.println("Welcome " + name + " and congrats on your new job as a " + positionString);
    }
}
