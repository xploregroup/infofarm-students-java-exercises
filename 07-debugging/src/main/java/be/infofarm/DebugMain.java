package be.infofarm;

import model.Employee;

import java.util.Scanner;

public class DebugMain {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String name;
        String position;

        System.out.println("Please enter your name");
        name = input.nextLine();

        System.out.println("Please enter your desired position (engineer, scientist, both)");
        position = input.nextLine();

        Employee newEmployee = new Employee(position, name);
        newEmployee.display();
    }
}
