package be.infofarm;

import be.infofarm.model.Employee;

public class ArrayMain {
    public static void main(String[] args) {

        Employee[] employees = new Employee[10];
        employees[0] = new Employee("Vincent", "Engineer");
        employees[1] = new Employee("Thomas", "Engineer");
        employees[2] = new Employee("Jeroen", "Engineer");
        employees[3] = new Employee("Federica", "Engineer");
        employees[4] = new Employee("Glenn", "Engineer");
        employees[5] = new Employee("Pieter", "Scientist");
        employees[6] = new Employee("Jan", "Scientist");
        employees[7] = new Employee("Barthold", "Scientist");
        employees[8] = new Employee("Jasper", "Scientist");
        employees[9] = new Employee("Jenny", "Scientist");

        MaxNames maxNames = new MaxNames();
        maxNames.printTwoLongestNames(employees);
    }
}
