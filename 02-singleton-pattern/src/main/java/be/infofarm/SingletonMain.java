package be.infofarm;

import be.infofarm.model.Infofarm;

public class SingletonMain {
    public static void main(String[] args) {
        Infofarm i1 = Infofarm.getInstance();
        Infofarm i2 = Infofarm.getInstance();

        // TODO: Should print "THERE CAN ONLY BE ONE INFOFARM" twice
        System.out.println(i1.message);
        System.out.println(i2.message);

        // TODO: Should print "therecanonlybeoneinfofarm" twice
        System.out.println(i1.message);
        System.out.println(i2.message);
    }
}
