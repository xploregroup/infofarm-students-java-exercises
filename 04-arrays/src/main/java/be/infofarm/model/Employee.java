package be.infofarm.model;

public class Employee {
    private String name;
    private String function;

    public Employee(String name, String function) {
        this.name = name;
        this.function = function;
    }

    public String getName() {
        return name;
    }

    public String getFunction() {
        return function;
    }
}
