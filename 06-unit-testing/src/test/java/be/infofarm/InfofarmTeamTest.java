package be.infofarm;

import be.infofarm.InfofarmTeam;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InfofarmTeamTest {
    private InfofarmTeam team;

    @BeforeEach
    public void setUp() throws Exception {
        team = new InfofarmTeam();
    }

    @Test
    @DisplayName("Hire new employee should work.")
    public void testHireEmployee() {

        // TODO: Enter code here

        fail();
    }

    @Test
    @DisplayName("Hire existing employee should work, but there should be no duplications.")
    public void testHireExistingEmployee() {

        // TODO: Enter code here

        fail();
    }

    @Test
    @DisplayName("Fire existing employee should work.")
    public void testFireExistingEmployee() {

        // TODO: Enter code here

        fail();
    }

    @Test
    @DisplayName("Fire non existing employe should throw IllegalArgumentException with the correct message.")
    public void testFireNonExistingEmployee() {

        // TODO: Enter code here

        fail();
    }

}
