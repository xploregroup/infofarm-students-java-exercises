package be.infofarm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class InfofarmTeam {
    Set<String> team = new HashSet<>(Arrays.asList("Vincent", "Johan", "Robin"));

    public void hireEmployee(String name) {
        this.team.add(name);
    }

    public void fireEmployee(String name) {
        if (team.contains(name)) {
            this.team.remove(name);
        } else {
            throw new IllegalArgumentException("Person does not exist in the team. Firing him/her makes no sense !");
        }
    }

    public Set<String> getTeam() {
        return this.team;
    }
}
